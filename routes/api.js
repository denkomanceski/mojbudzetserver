var express = require('express');
var router = express.Router();
var sqlAcessor = require('../core/sqlAccessor');
/* GET users listing. */
router.post('/login', (req, res) => {
    var body = req.body;
    sqlAcessor.validateUsername(body.email, body.password, (user) => {
        (user.ok)
            ? res.send({data: user.data})
            : res.send({data: false})
    })
});
var multer = require('multer')
var path = require('path');
var mkdirp = require('mkdirp');

var createExistsFolder = (path) => {
    return new Promise((resolve, reject) => {
        mkdirp(path, function (err) {
            if (err) console.error(err)
            else console.log('pow!')
            resolve(!!err)
        });
    })
}
// var upload = multer({dest: path.join(__dirname, '../photos/')})
/* GET home page. */
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        console.log(req.body.billId, "ttt");
        // req.body.payload = JSON.parse(req.body.payload);
        var billPath = path.resolve(__dirname, `../photos/bills/${req.body.userId}`);
        createExistsFolder(billPath).then(err => {
            if(err) throw  err;
            else {
                cb(null, billPath)
            }
        });
    },
    filename: function (req, file, cb) {
        cb(null, `${req.body.billId}.jpg`);
    }
});
var uploads = multer({storage: storage});
router.post('/uploadBill', uploads.any(), (req, res) => {
    console.log("Image came..", JSON.stringify(req.body));
    req.body.generatedUrl = `/photos/${req.body.app}/${req.body.userId}_${req.body.photoFor}.jpg`;
    sqlAcessor.insertPhotoForRacun(req.body, (data) => {
        res.send({success: true})
    });
});
// router.post('/billPhoto', (req, res) => {
//     sqlAcessor.insertPhotoForRacun(req.body, (data) => {
//         res.send(data);
//     })
// });
router.post('/feedback', (req, res) => {
    sqlAcessor.sendFeedback(req.body.userId, req.body.comment, (data) => {
        res.send(data);
    })
})
router.post('/bill', (req, res) => {
    var racun = req.body;
    sqlAcessor.insertRacun(racun, (data) => {
        res.send(data);
    })
});
router.post('/register', (req, res) => {
    sqlAcessor.registerUser(req.body, (data) => {
        res.send(data);
    })
});
router.post('/updateDescription', (req, res) => {
    sqlAcessor.updateDescription(req.body, (data) => {
        res.send(data);
    })
})
router.get('/billPhoto/:racunId', (req, res) => {
    sqlAcessor.fetchImage(req.params.racunId, (data) => {
        res.send(data);
    })
});
router.get('/bills/:userId', (req, res) => {
    sqlAcessor.fetchDataForUserId(req.params.userId, (data) => {
        res.send(data);
    })
});
router.get('/deleteBill/:racunId', (req, res) => {
    sqlAcessor.deleteRacun(req.params.racunId, (data) => {
        res.send(data);
    })
});


router.get('/', (req, res) => {
    res.send('respond with a resource');
});


module.exports = router;
