/**
 * Created by denko on 4/2/2016.
 */
var mysql = require('mysql');
var uuid = require('node-uuid');
var moment = require('moment');
var months = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"];
var dbConnection = mysql.createConnection({
    //host: 'localhost',
    host: '46.101.172.70',
    //user: 'root',
    user: 'root',
    password: 'razerqwe123',
    database: 'mojbudzet',
    //stream: sockConn
});

function blobToBase64(blob) {
    var buffer = new Buffer(blob);
    return buffer.toString('base64');
}
function base64toBlob(base64) {
    return new Buffer(base64, 'base64');
}
function transformByMonth(bills) {
    var structure = {};
    var companies = {};
    var firstYear = '';
    bills.forEach((bill) => {
        //Structure
        var date = new Date(bill.timestamp);
        var month = months[date.getMonth()];
        var year = date.getFullYear();
        if (!firstYear)
            firstYear = year;
        if (!structure[year]) {
            structure[year] = {total: 0};
        }
        if (!structure[year][month]) {
            structure[year][month] = {amount: 0};
        }
        structure[year].total += bill.amount;
        // structure[year].total = structure[year].total.toFixed(2);
        // structure[year].total = parseFloat(structure[year].total).toFixed(2);
        structure[year][month].amount += bill.amount;
        // structure[year][month].amount = parseFloat(structure[year][month].amount).toFixed(2);
        // structure[year][month].bills.push(bill);
        //Companies
        if (!companies[bill.company])
            companies[bill.company] = {amount: 0};
        companies[bill.company].amount += bill.amount;
        // companies[bill.company].amount = companies[bill.company].amount.toFixed(2);
    });
    //Sorting checking companies
    var companiesArray = [];
    for (var comp in companies) {
        companiesArray.push({name: comp, amount: companies[comp].amount});
    }
    var companiesArrayLength = companiesArray.length;
    for (var i = 0; i < companiesArrayLength; i++) {
        for (var j = i + 1; j < companiesArrayLength; j++) {
            if (companiesArray[i].amount < companiesArray[j].amount) {
                var temp = companiesArray[j];
                companiesArray[j] = companiesArray[i];
                companiesArray[i] = temp;
            }
        }
    }
    //Generating chart data
    var labels = [];
    var data = [];
    for (var month in structure[firstYear]) {
        if (month != 'total') {
            labels.push(month);
            data.push(structure[firstYear][month].amount.toFixed(2));
        }
    }
    var chartData = {
        labels,
        datasets: [
            {
                label: firstYear,
                fillColor: "rgba(0, 193, 159, 0.5)",
                strokeColor: "rgba(0, 193, 159, 0.8)",
                highlightFill: "rgba(0, 193, 159, 0.75)",
                highlightStroke: "rgba(0, 193, 159, 1)",
                data
            }
        ]
    }
    //Calculating by year
    var yearStatistic = [];
    for (var year in structure) {
        yearStatistic.push({year, total: structure[year].total.toFixed(2)});
        structure[year].total = structure[year].total.toFixed(2);
    }

    //labels: ["January", "February", "March", "April", "May", "June", "July"],
    //    datasets: [
    //    {
    //        label: "My First dataset",
    //        fillColor: "rgba(220,220,220,0.5)",
    //        strokeColor: "rgba(220,220,220,0.8)",
    //        highlightFill: "rgba(220,220,220,0.75)",
    //        highlightStroke: "rgba(220,220,220,1)",
    //        data: [65, 59, 80, 81, 56, 55, 40]
    //    }
    //]


    return {annualReports: structure, companiesReports: companiesArray, chartData, yearStatistic};
}
function serverInfoGen() {
    return {}
}
var sendFeedback = function (userId, comment, cb) {
    dbConnection.query('INSERT INTO feedback (userId, comment, timestamp) values (?,?,?)', [userId, comment, new Date()], (err, rows) => {
        cb({ok: true})
    });
}

var fetchDataForUserId = (userId, cb) => {
    var data = {};
    dbConnection.query('SELECT * FROM users WHERE id = ? LIMIT 1', [userId], (err, rows) => {
        if (err) throw err;
        if (rows.length > 0) {
            delete rows[0].password;
            data.info = rows[0];
            dbConnection.query('SELECT id, userId, photoUrl, timestamp, amount, company, description FROM racuni WHERE userId = ? ORDER BY timestamp DESC LIMIT 300', [userId], (err, rows) => {
                if (err) throw err;
                if (rows.length > 0) {
                    data.formed = transformByMonth(rows);
                    data.serverInfo = serverInfoGen();
                    // rows.forEach(row => {
                    //     if (row.racunPhoto)
                    //         row.racunPhoto = blobToBase64(row.racunPhoto);
                    // });
                    data.bills = rows;
                }
                cb(data);
            })
        }
        else {
            cb({});
        }

    });

};
var validateUsername = (email, password, cb) => {
    dbConnection.query('SELECT * FROM users WHERE email = ? AND password = ?', [email, password], (err, rows, fields) => {
        if (err) throw err;
        if (rows.length > 0) {
            delete rows[0].password;
            cb({ok: true, data: rows[0]})
        }
        else
            cb({ok: false});
    });
}
var checkExistingUsername = (email) => {
    return new Promise((resolve, reject) => {
        dbConnection.query('SELECT * FROM users WHERE email = ?', [email], (err, rows, fields) => {
            if (err) throw err;
            if (rows.length > 0) {
                resolve({exists: true})
            }
            else
                resolve({exists: false});
        });
    })

}
var registerUser = (user, cb) => {
    checkExistingUsername(user.email)
        .then(result => {
            if (result.exists) {
                cb({ok: false, reason: "Ta email že obstaja!"})
            } else {
                user.id = uuid.v4();
                dbConnection.query('INSERT INTO users (id, fullName, email, password) values (?,?,?,?)', [user.id, user.fullName, user.email, user.password], (err, rows, fields) => {
                    if (err) throw err;
                    cb({ok: true, userId: user.id});
                });
            }
        })

};
var insertRacun = (racun, cb) => {
    racun.id = uuid.v4();
    racun.timestamp = new moment(racun.timestamp).format('YYYY-MM-DD HH:mm:ss');
    dbConnection.query('INSERT INTO racuni (id, userId, timestamp, amount, company, description) values (?,?,?,?,?,?)', [racun.id, racun.userId, racun.timestamp, racun.amount, racun.company, racun.description], (err, rows, fields) => {
        if (err) throw err;
        cb({ok: true, racunId: racun.id});
    });
};
var insertPhotoForRacun = (racun, cb) => {
    dbConnection.query('UPDATE racuni SET photoUrl = ? WHERE id = ?', [racun.generatedUrl, racun.billId], (err, rows, fields) => {
        if (err) throw err;
        cb({ok: true});
    });
};
var deleteRacun = (racunId, cb) => {
    dbConnection.query('DELETE FROM racuni WHERE id = ?', [racunId], (err, rows, fields) => {
        if (err) throw err;
        cb({ok: true});
    });
};
var fetchImage = (racunId, cb) => {
    dbConnection.query('SELECT id, photoUrl FROM racuni WHERE id = ?', [racunId], (err, rows, fields) => {
        if (err) throw err;
        if (rows.length > 0) {
            cb({ok: true, racunPhoto: rows[0].photoUrl});
        }
        else
            cb({ok: true});
    });
};
var updateDescription = (racun, cb) => {
    dbConnection.query('UPDATE racuni SET description = ? WHERE id = ?', [racun.description, racun.id], (err, rows, fields) => {
        if (err) throw err;
        cb({ok: true});
    });
}
exports.sendFeedback = sendFeedback;
exports.registerUser = registerUser;
exports.insertRacun = insertRacun;
exports.insertPhotoForRacun = insertPhotoForRacun;
exports.deleteRacun = deleteRacun;
exports.fetchDataForUserId = fetchDataForUserId;
exports.validateUsername = validateUsername;
exports.fetchImage = fetchImage;
exports.updateDescription = updateDescription;